`#ES Lint file to use for all editors

This repo contains two dot files, .eslintrc.json and .editorconfig. Both files should live in the project root directory.

The ES Lint requires node and the eslint node module to be installed globally. The editor config requires a plugin for all editors, with the exceptiion of IntelliJ.

## ESLint
### Install Node
The easiest way to install node and manage it is using Node Version Manager (nvm)
* [nvm](https://github.com/creationix/nvm)

1. Install nvm: `curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.4/install.sh | bash`
2. Open a new terminal or iTerm window and type nvm. If the command is not found, you just need to add the following to most likely your .bash_profile. If not it will be one of these files \*~/.bash_profile, ~/.zshrc, ~/.profile, or ~/.bashrc\*

```
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm
```

3. Install node using nvm: `nvm install node` (this will install the latest version of node)
4. Install eslint globally: `npm install eslint -g`

### Intellij
1. Go to Preferences
2. Search for eslint
3. Check the enable box

    ![Example](./screen-captures/intellij/intellij-eslint-config-options.png)


### SublimeText 3
1. Open Sublime
2. Type `cmd + p`
3. Search and select `Install Package`
4. Search and Select `Sublime Linter`
5. Restart Sublime
6. Type `cmd + p`
7. Search and Select `Install Package`
8. Search and Install `SublimeLinter-contrib-eslint`
9. Restart Sublime
10. Go to the "Tools" menu
11. Select Sublime Linter and choose your settings

### VS Code
1. Install the ES Link Extension
2. Reload Window
3. Add `"eslint.enable": true` to the User or Workplace settings

## EditorConfig
The .editorconfig file does some nifty tasks, like making sure our tabs are always 4, removes trailing white space, adds and additional line to the end of the file. It does this without any effort from us, all we have to do is save the file.

If the .editorconfig file is not in your project, add it to the root directory and follow your editor instrcutions below.

### IntelliJ
You're good to go! IntelliJ has the Editor Config functionality without the need for any plugins.

### SublimeText
1. In sublime, `cmd + p`
2. Search and select `Install Package`
3. Search `edit`
4. Select `EditorConfig`
5. Restart Sublime

### Visual Studio Code
1. Install the Editor Config extension
2. Reload Window
